from matplotlib import pyplot as plt

import sys, os, copy, pickle, string, itertools, tqdm, math

import pandas as pd
import numpy as np
import scipy as spy
import seaborn as sns

from glob import glob

from matplotlib.ticker import ScalarFormatter
from collections import Counter, defaultdict

#from sklearn.metrics import classification_report, r2_score, roc_curve, auc
#from sklearn.preprocessing import StandardScaler, MinMaxScaler, scale, minmax_scale
#from sklearn.linear_model import RidgeCV, LassoCV, RANSACRegressor, LinearRegression
#from sklearn.mixture import GaussianMixture, BayesianGaussianMixture
#from sklearn.decomposition import PCA


from multiprocessing import Pool
from functools import partial

from scipy.sparse import csr_matrix
from scipy.stats import spearmanr, pearsonr
from scipy.special import softmax
from scipy.interpolate import interp1d


import matplotlib.ticker as ticker
from matplotlib.ticker import FuncFormatter
from matplotlib.colors import LinearSegmentedColormap
from matplotlib import colors



from scipy.integrate import odeint
from scipy.interpolate import interp1d
from scipy.integrate import ode

import multiprocessing as mp

from FGraphLib import *