import numpy as np
import scipy as spy
from scipy.integrate import odeint
from scipy.interpolate import interp1d
from scipy.integrate import ode   
from functools import partial
from scipy.integrate import solve_bvp
from copy import copy, deepcopy
from matplotlib import pyplot as plt


########################################## EPIDEMIC MODEL  ##########################################
########################################## EPIDEMIC MODEL  ##########################################
########################################## EPIDEMIC MODEL  ##########################################
########################################## EPIDEMIC MODEL  ##########################################
class parameters():
    def __init__(self, **kwargs):        
        self.h0 = 0.5
        self.h1 = 0.9
        self.boundOverPolicy = 0.3
        self.boundUnderPolicy = 0.3
        
        self.b0 = 5
        self.alpha = .2
        self.gamma = 1
        self.eps = 1
        self.cN = 0.75
        
        for key, value in kwargs.items():
            assert key in vars(self), "Invalid parameter: " + key
            setattr(self, key, value)
            
            
            
class modelEpidemic():
    
    def __init__(self, par):
        self.par=deepcopy(par)
        self.setU01()
        self.setState01()
        self.setBoundControl()
    
    def setU01(self):
        h0, h1, b0, alpha, gamma, eps, cN = self.par.h0, self.par.h1, self.par.b0, self.par.alpha, self.par.gamma, self.par.eps, self.par.cN
        self.u0 = cN*h0
        self.u1 = cN*h1
     
    def setState01(self):
        self.x0 = np.array(self.getSS(self.u0))
        self.x1 = np.array(self.getSS(self.u1))
    
    
    def __str__(self):
        new_line = '\n'
        return f"EM:{new_line}h0={self.par.h0:.3f}(u0={self.u0:.3f})(x0={self.x0}){new_line}h1={self.par.h1:.3f}(u1={self.u1:.3f})(x1={self.x1})"
     
    def __repr__(self):
        par = self.par
        new_line = '\n'
        
        return f"""
                h0: {par.h0:.4f}
                h1: {par.h1:.4f}
                boundOverPolicy: {par.boundOverPolicy:.4f}
                boundUnderPolicy: {par.boundUnderPolicy:.4f}
                b0: {par.b0:.4f}
                alpha: {par.alpha:.4f}
                gamma: {par.gamma:.4f}
                eps: {par.eps:.4f}
                cN: {par.cN:.4f}
                u0: {self.u0:.4f}
                u1: {self.u1:.4f}
                x0: {self.x0[0]:.4f},{self.x0[1]:.4f}
                x1: {self.x1[0]:.4f},{self.x1[1]:.4f}
                """

    
    def setBoundControl(self):
        h0, h1, b0, alpha, gamma, eps, cN = self.par.h0, self.par.h1, self.par.b0, self.par.alpha, self.par.gamma, self.par.eps, self.par.cN
        u0, u1 = self.u0, self.u1
        
        if u1>u0: 
            self.uUBound = u1 + (u1-u0)*self.par.boundOverPolicy
            self.uLBound = u0 - (u1-u0)*self.par.boundUnderPolicy
        else: 
            self.uUBound = u0 + (u1-u0)*self.par.boundUnderPolicy
            self.uLBound = u1 - (u0-u1)*self.par.boundOverPolicy
        
    
    def getSS(self, u=0):
        b0, alpha, gamma, eps, cN = self.par.b0, self.par.alpha, self.par.gamma, self.par.eps, self.par.cN
        h0, h1 = self.par.h0, self.par.h1
        u0, u1 = self.u0, self.u1
        zero = 1e-3
        c = cN - u 
        
        ## x1
        if b0 < 1: return [zero, zero]
        ## x3
        if (c>(1-1/b0) or c>=1) and b0 > 1: return [1 - 1/b0, zero]
        ## x4
        if c<(b0*alpha - 1)/(b0*alpha): return [1 - 1/(b0*alpha), 1]
        ## x5
        if b0>1 and b0<=1/alpha and c<(b0-1)/b0:
            I = c
            P = (b0 - 1 - b0*c)/(b0*(c-1)*(alpha-1))
            return [I, P]
        if b0>1/alpha and c<(b0-1)/b0 and c<(b0*alpha-1)/b0*alpha:
            I = c
            P = (b0 - 1 - b0*c)/(b0*(c-1)*(alpha-1))
            return [I, P]
    

    def rhsState(self, x_k, u_k):
        b0, alpha, gamma, eps, cN = self.par.b0, self.par.alpha, self.par.gamma, self.par.eps, self.par.cN
        h0, h1 = self.par.h0, self.par.h1
        u0, u1 = self.u0, self.u1
        u, h = u1, h1

        I = x_k[0]
        P = x_k[1]
        ut = u_k[0]
        
        Delta = I - (cN - u - ut)
        beta = b0*((1-P) + alpha*P)

        rhsI = beta*I*(1-I) - gamma*I
        rhsP = eps*P*(1-P) * Delta

        return np.array([rhsI, rhsP])



    def rhsLambda(self, x_k, u_k, lamb_k):
        b0, alpha, gamma, eps, cN = self.par.b0, self.par.alpha, self.par.gamma, self.par.eps, self.par.cN
        h0, h1 = self.par.h0, self.par.h1
        u0, u1 = self.u0, self.u1
        JF, PF = self.x1[0], self.x1[1]            
        u, h = u1, h1
        
        I = x_k[0]
        P = x_k[1]
        ut = u_k[0]
        lamb1 = lamb_k[0]
        lamb2 = lamb_k[1]

        J = I
        a = alpha
        #original
        #rhsILamb1 = - (2*h*J + (-1-b0*(-1+2*J)*(1+P*(-1+alpha)))*lamb1 - (-1+P)*P*eps*lamb2)
        #rhsILamb2 = - (-b0*(-1+J)*J*(-1+alpha)*lamb1 - (-1+2*P)*(-cN+J+u+ut)*eps*lamb2)

        #original
        rhsILamb1 = - (J - JF + 2*b0*J*(-1+P-a*P)*lamb1 + (-1+b0+(-1+a)*b0*P)*lamb1 - (-1+P)*P*eps*lamb2)
        rhsILamb2 = - (P - PF + b0*J*(-1+a+J-a*J)*lamb1 + (-cN+J)*eps*lamb2 + (2*cN*P + u + ut - 2*P*(J+u+ut))**eps*lamb2)
        
        return np.array([rhsILamb1, rhsILamb2])

    
    
    def jacobian(self, X, uStar):
        x_k = X[[0, 1]]
        lamb_k = X[[2,3]]
        I = x_k[0]
        P = x_k[1]
        ut = uStar[0]
        lamb1 = lamb_k[0]
        lamb2 = lamb_k[1]
                
        b0, alpha, gamma, eps, cN = self.par.b0, self.par.alpha, self.par.gamma, self.par.eps, self.par.cN
        h0, h1 = self.par.h0, self.par.h1
        u0, u1 = self.u0, self.u1
        u, h = u1, h1
        J = I
        a = alpha
        
        #derivate dJ
        ddJ = np.array([-1-b0*(-1+2*J)*(1+(-1+a)*P), -((-1+a) * b0*(-1+J)*J), np.zeros(ut.shape), np.zeros(ut.shape)])
        
        #derivate dP
        ddP = np.array([-((-1+P)*P*eps), (1-2*P)*(-cN + J + u + ut)*eps, np.zeros(ut.shape), np.zeros(ut.shape)])
                       
        dLam1 = np.array(
            [-2*h + 2*b0*(1+(-1+a)*P)*lamb1,
             (-1+a)*b0*(-1+2*J)*lamb1 + (-1 + 2*P)*eps*lamb2, 
             1 + b0*(-1+2*J)*(1 + (-1+a)*P),
             (-1 + P)*P*eps])
                
        dLam2 = np.array(    
            [(-1+a)*b0*(-1 + 2*J)*lamb1 + (-1 + 2*P)*eps*lamb2,
            2*(-cN+J+u+ut)*eps*lamb2, 
             (-1+a)*b0*(-1+J)*J,
             (-1+2*P)*(-cN+J+u+ut)*eps])
        
        
        ret = np.dstack([ddJ, ddP, dLam1, dLam2])
        
        return ret.transpose((2,0,1))
        return ret.transpose((0,2,1))
    
    
    def optimality_condition(self, x_k, lamb_k, t):
        b0, alpha, gamma, eps, cN = self.par.b0, self.par.alpha, self.par.gamma, self.par.eps, self.par.cN
        h0, h1 = self.par.h0, self.par.h1
        u0, u1 = self.u0, self.u1
        u, h = u1, h1
        
        J,P = x_k[0], x_k[1]
        lamb1,lamb2 = lamb_k[0],lamb_k[1]

        #original
        #u_star = -u - ((-1+P)*P*eps*lamb2)/(2*(-1+h))
        
        #three terems
        u_star = (-1+P)*P*eps*lamb2
         
        u_star = np.minimum(self.uUBound-u1, u_star)
        u_star = np.maximum(self.uLBound-u1, u_star)
        return np.array([u_star,])


    
    
    
    
    

    
##########################################  SOLVER PONTRYAGIN  ##########################################
##########################################  SOLVER PONTRYAGIN  ##########################################
##########################################  SOLVER PONTRYAGIN  ##########################################
##########################################  SOLVER PONTRYAGIN  ##########################################


class dataEvol:
    """
    Class to save the time-series
    """
    
    def __init__(self, t, dimX=2, dimU=1):
        nStep = t.size
        
        self.t = t
        self.tBackward = np.flip(self.t)
        
        self.x = np.zeros((nStep, dimX))
        self.u = np.zeros((nStep, dimU))
        self.lamb = np.zeros((nStep, dimX))
        
    def getDataAtTime(self, t):
        """
        Get data (x, u, lamb) at any time t by intrpolate the timeseries
        """
        x = interp1d(self.t, self.x, fill_value="extrapolate", axis=0)(t)
        u = interp1d(self.t, self.u, fill_value="extrapolate", axis=0)(t)
        lamb = interp1d(self.t, self.lamb, fill_value="extrapolate", axis=0)(t)
        return x, u, lamb
    
    def getContinousU(self):
        return interp1d(self.t, self.u, fill_value="extrapolate", axis=0)
    
    
    def __add__(self, other):
        ret = dataEvol(np.hstack([self.t, other.t]))
        ret.x = np.vstack([self.x, other.x])
        ret.u = np.vstack([self.u, other.u])
        ret.lamb = np.vstack([self.lamb, other.lamb])
        
        return ret


    
    
    
class solverBasic():
    def __init__(self, EM, eps=.001, nStep=1000, t1=15.0, weightAtX1=1000, verbose=0):
        self.EM = EM 
        self.nStep = nStep
        self.eps = eps
        self.weightAtX1 = weightAtX1
        self.verbose = verbose
        
        t = np.linspace(0, t1, nStep)
        self.h = t[1]-t[0]
        
        self.evol = dataEvol(t)
        self.evolFree = dataEvol(t)
        
        

    def freeRun(self, tol=1e-3):
        """
        Run until convergence to SS
        """
        def f(t, x):
            return self.EM.rhsState(x, [0, 0])
                
        solver = ode(f).set_integrator("dop853")
        solver.set_initial_value(self.EM.x0)
        
        xs = [self.EM.x0, ]
        ts = [0, ]
        t = self.h
        
        while np.max(np.abs(xs[-1] - self.EM.x1))>tol:
            ts.append(ts[-1] + self.h)
            solver.integrate(ts[-1])
            xs.append(solver.y)
            
            if not solver.successful(): 
                print(f"error in ode solver (t={t})")
                return None
    
        self.evolFree = dataEvol(np.array(ts))
        self.evolFree.x = np.array(xs)
        

    
class solverWithBVP(solverBasic):
                                       
    def rhsAll(self, t, X):
        x = X[[0, 1]]
        lamb = X[[2,3]]
        
        uStar = self.EM.optimality_condition(x, lamb, t)
        xDot = self.EM.rhsState(x, uStar)
        lambDot = self.EM.rhsLambda(x, uStar, lamb) 
        
        return np.vstack([xDot, lambDot])

    
    def bc(self, xa, xb):
        termValLamb0 = xb[0] - self.EM.x1[0]
        termValLamb1 = xb[1] - self.EM.x1[1]
        
        termValLamb0 *= self.weightAtX1
        termValLamb1 *= self.weightAtX1
    
        return np.array([xa[0]-self.EM.x0[0], xa[1]-self.EM.x0[1], xb[2], xb[3]])
        #return np.array([xa[0]-self.EM.x0[0], xa[1]-self.EM.x0[1], xb[2]-termValLamb0, xb[3]-termValLamb1])
        
                 
    def getInitialGuess(self):
        initGuess = copy(self.evol)
        initGuess.u *= 0
        
        initGuess.x = self.solveState(initGuess.t, initGuess.u, self.EM.x0)
        initGuess.lamb = self.solveLambda(initGuess.t, initGuess.lamb, initGuess.u, self.EM.x0, [0, 0])
        
        return initGuess
    
    
    def solve(self, initGuess=None):
        if initGuess is None: initGuess = self.getInitialGuess()
        else: initGuess = copy(initGuess)
        
        X = np.hstack([initGuess.x, initGuess.lamb]).transpose()
        res = solve_bvp(self.rhsAll, self.bc, initGuess.t, X, max_nodes=10_000, tol=1e-3, verbose=self.verbose)
        X = res.sol(initGuess.t)
        
        initGuess.x = X[[0, 1]].transpose()
        initGuess.lamb = X[[2,3]].transpose()
        initGuess.u = self.EM.optimality_condition(X[[0, 1]], X[[2,3]], initGuess.t).transpose()
        initGuess.x = self.solveState(initGuess.t, initGuess.u, initGuess.x[0])
        
        self.evol = initGuess
        self.success = res.success
        