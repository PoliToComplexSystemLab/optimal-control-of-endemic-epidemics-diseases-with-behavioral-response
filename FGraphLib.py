import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
import string




def plotSS(ax, ME, axLimit=False, **kwargs):
    w = np.arange(0, 1, 0.1)
    ss = np.array([ME.getSS(_w) for _w in w])

    ax.plot(ss[:, 0], ss[:, 1], c="black", alpha=0.2, **kwargs)
        
    ax.scatter(*ME.x0, c="red", s=10, **kwargs)
    ax.scatter(*ME.x1, c="green", s=10, **kwargs)
    
    if axLimit:
        xLow = min(ME.x0[0], ME.x1[0]) * (1-0.2)
        xMax = max(ME.x0[0], ME.x1[0]) * (1+0.2)
        yLow = min(ME.x0[1], ME.x1[1]) * (1-0.2)
        yMax = max(ME.x0[1], ME.x1[1]) * (1+0.2)
        ax.set_xlim(xLow, xMax)
        ax.set_ylim(yLow, yMax)
        
    if not axLimit:
        ax.set_xlim(-0.01, 1)
        ax.set_ylim(-0.01, 1)


def plotTraj(ax, FBS, **kwargs):
    ax.plot(FBS.x[:, 0], FBS.x[:, 1], **kwargs)
    
def plotTrajFree(ax, FBS, **kwargs):
    ax.plot(FBS.xFree[:, 0], FBS.xFree[:, 1], **kwargs)
    

################  BASE  ################
################  BASE  ################

def betterDate(ax):
    if not isinstance(ax, np.ndarray):
        ax = [ax,]
        
    locator = mdates.AutoDateLocator(minticks=3, maxticks=11)
    formatter = mdates.ConciseDateFormatter(locator, show_offset=False)
    for _ax in ax:
        _ax.xaxis.set_major_locator(locator)
        _ax.xaxis.set_major_formatter(formatter)
    
    
def betterNumbers(ax):
    if not isinstance(ax, np.ndarray):
        ax = [ax,]
        
    for _ax in ax: _ax.yaxis.set_label_coords(-0.1, 0.5)
    for _ax in ax: _ax.ticklabel_format(axis='y', style='sci', scilimits=(0,0))

        
def betterPanelLabel(ax, startingLetter=0, offset=[0,0]):
    if not isinstance(ax, np.ndarray):
        ax = np.array([ax,])
 
    ax = np.ravel(ax)
    for _ax,letter in (zip(ax, string.ascii_uppercase[startingLetter:])):
        xpos = -0.1 + offset[0]
        ypos = 1.15 + offset[1]
        _ax.text(xpos, ypos, letter, transform=_ax.transAxes, fontsize=12, fontweight='bold', va='top', ha='right')


def betterAx(nRow, nCol, sharex=True):
    hspace=0.25
    wspace=0.25
    hRow= 2.3
    wCol = 4.3
    
    fig, ax = plt.subplots(nRow, nCol, sharex=sharex, figsize=(nCol*wCol + ((nCol-1)*wspace*wCol), nRow*hRow + ((nRow-1)*hspace*hRow)), dpi=100)
    plt.subplots_adjust(hspace=hspace)
    
    return fig, ax