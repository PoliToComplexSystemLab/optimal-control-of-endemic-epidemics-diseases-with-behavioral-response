# Optimal control of endemic epidemics diseases with behavioral response
The code used in the paper "Optimal control of endemic epidemics diseases with behavioral response".  
This standalone package contains the code and the dataset sufficient to reproduce all the quantitative results in the manuscript.

# Instructions
The code for the simulation is written in `PYTHON (Version: 3.7.7)` and can be used through ipython notebooks  
The package has been tested on `MacOS 10.15 `and `Ubuntu 18.04.4 LTS` 


# Licence
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type"> Optimal control of endemic epidemics diseases with behavioral response </span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.